public class ZomatoRestaurant
{

    //Zomato URL
    private String url ;
    //Name of Restaurant
    private String name	;
    //Address	
    private String address ;
    //Location	
    private String location ;
    //Cuisine	
    private String cuisine ;
     //Top Dishes	
    private String topDishes ;
    //Price for 2	
    private float priceFor2;
    //Ratings	
    private float ratings ;
    //No of Votes
    private int noOfVotes ;
  
    public ZomatoRestaurant()
    {
    	
    }
  public ZomatoRestaurant(String url, String name, String address, String location, String cuisine, String topDishes, float priceFor2, float ratings,
			int noOfVotes)
            {
                
		this.url = url;
		  this.name = name;
			this.address = address;
			this.location = location;
			this.cuisine = cuisine;

		this.topDishes = topDishes;
		this.priceFor2 = priceFor2;
		this.ratings = ratings;
		this.noOfVotes = noOfVotes;
      
            }
     
public String geturl() {
		return url;
	}



	public void seturl(String url) {
		this.url = url;
	}



	public String getname() {
		return name;
	}



	public void setname(String name) {
		this.name = name;
	}



	public String getaddress() {
		return address;
	}



	public void setaddress(String address) {
		this.address = address;
	}



	public String getlocation() {
		return location;
	}



	public void setlocation(String location) {
		this.location = location;
	}



	public String getcuisine() {
		return cuisine;
	}



	public void setcuisine(String cuisine) {
		this.cuisine = cuisine;
	}



	public String gettopDishes() {
		return topDishes;
	}



	public void settopDishes(String topDishes) {
		this.topDishes = topDishes;
	}



	public float getpriceFor2() {
		return priceFor2;
	}



	public void setpriceFor2(float priceFor2) {
		this.priceFor2 = priceFor2;
	}



	public float getratings() {
		return ratings;
	}



	public void setratings(float ratings) {
		this.ratings = ratings;
	}



	public int getnoOfVotes() {
		return noOfVotes;
	}



	public void setnoOfVotes(int noOfVotes) {
		this.noOfVotes = noOfVotes;
	}

	
}