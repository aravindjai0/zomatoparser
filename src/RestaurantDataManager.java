public class RestaurantDataManager {

   
    public ZomatoRestaurant getZomatoRestaurant(String data, String delimiter) {

      Parser parser = ParserFactory.getInstance().getParser(DelimitedCharacterParser.TAB);

      String[] cols = parser.parse(data, delimiter);

      ZomatoRestaurant info = new  ZomatoRestaurant();
        info.seturl(cols[0]);
        info.setname(cols[1]);
        info.setaddress(cols[2]);
	    info.setlocation(cols[3]);
	    info.setcuisine(cols[4]);
	    info.settopDishes(cols[5]);
	    info.setpriceFor2(Float.parseFloat(cols[6]));

        String rating = (String)cols[7];
	        if (rating.equals("invalid")) {
	        	rating = "0";
	        }
	        float floatRating = Float.parseFloat(rating);
	        info.setratings(floatRating);
	        
	        
	        String votes = (String)cols[8];
	        if (votes.equals("invalid")) {
	        	votes = "0";
	        }
	        int intVotes = Integer.parseInt(votes);
	        info.setnoOfVotes(intVotes);
	        
	        return info;
	}

	public ZomatoRestaurant getData(String data, String tab) {
		// TODO Auto-generated method stub
		return null;
	}
}

	       
	      