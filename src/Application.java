
import java.io.File;
import java.util.Scanner;
public class Application 
{
	 public static void main (String[] args) throws Exception 
	 {
	
 //getting path
    System.out.println("enter the path:");
     String filepath = new Scanner(System.in).nextLine();

        File tsvFile = new File(filepath);
		Scanner scan = new Scanner(tsvFile);
		
		while (scan.hasNext()) 
		{
			
			String row = scan.nextLine();
            RestaurantDataManager restDataManager = new RestaurantDataManager();
            ZomatoRestaurant restInfo = restDataManager.getZomatoRestaurant(row, DelimitedCharacterParser.TAB);
            //add data into dataset
           
             FilterData newSearch = new FilterData();

            newSearch.searchData(restInfo.gettopDishes(), restInfo.getname(),restInfo.geturl(), restInfo.getpriceFor2(),500,restInfo.getratings(),4);
        }
		scan.close();
    }
}